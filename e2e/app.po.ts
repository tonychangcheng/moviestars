import { browser, by, element } from 'protractor';

export class AppPage {
  actors = element.all(by.className('actor'));
  actorWilWheaton = this.actors.get(4);
  actorWilWheatonName = this.actorWilWheaton.element(by.tagName('h4'));
  actorWilWheatonRoles = this.actorWilWheaton.all(by.className('role'));
  actorWhiWheatonFirstRoleName = this.actorWilWheatonRoles.get(0);

  navigateTo() {
    return browser.get('/');
  }
}
