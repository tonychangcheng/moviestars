import { AppPage } from './app.po';

describe('movie-stars App', () => {
  let page: AppPage;

  beforeEach(async () => {
    page = new AppPage();
  });

  it('should display actors and their roles', async () => {
    page.navigateTo();
    expect(await page.actors.count()).toEqual(17);
    expect(await page.actorWilWheatonName.getText()).toEqual('Wil Wheaton');
    expect(await page.actorWilWheatonRoles.count()).toEqual(2);
    expect(await page.actorWhiWheatonFirstRoleName.getText()).toEqual('Gorgie Lachance (Stand By Me)')
  });
});
