import { Component, OnInit } from '@angular/core';
import { NameListService, ViewActorModel } from './name-list.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  errorMessage: string;
  actors: ViewActorModel[] = [];

  constructor(private nameListService: NameListService) {}

  /**
   * Get the names OnInit
   */
  ngOnInit() {
    this.getNames();
  }

  /**
   * Handle the nameListService observable
   */
  getNames() {
    this.nameListService.get()
      .subscribe(
        actors => {
          this.actors = actors;
        },
        error => this.errorMessage = <any>error
      );
  }

}
