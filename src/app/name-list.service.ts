import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { from } from 'rxjs/observable/from';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/groupBy';
import 'rxjs/add/operator/catch';
import * as underscore from 'underscore';

interface RoleModel {
  name: string;
  actor: string;
}
interface MovieModel {
  name: string;
  roles: RoleModel[];
}
export class ActorModel {
  constructor(public actor: string, public roleName: string, public movieName: string) {}
}
export class ViewActorModel {
  constructor(public actorName: string, public roles: ActorModel[]) {}
}

@Injectable()
export class NameListService {

  constructor(private http: Http) { }
  /**
   * Returns an Observable for the HTTP GET request for the JSON resource.
   * @return {string[]} The Observable for the HTTP request.
   */
  lambdaData(movies: MovieModel[]): ViewActorModel[] {
    let viewActors: ViewActorModel[] = [];

    let validActors = movies.map((aMovie: MovieModel) => {
      let movieName = aMovie.name;
      // generate an array for the actors
      let actors = aMovie.roles.map((aRole: RoleModel) => {
        return new ActorModel(aRole.actor,aRole.name,movieName);
      });
      return actors;
    }).reduce((actorList: ActorModel[], aMovieActors: ActorModel[]) => {
      // combine all them to be one array
      return actorList.concat(aMovieActors);
    }).filter((aActor: ActorModel) => {
      // remove the invalid records
      return aActor.actor && aActor.movieName && aActor.roleName;
    });
    console.log('1d actors array');
    console.log(validActors);
    let groupedActors = underscore.groupBy(validActors, (aActor: ActorModel) => aActor.actor);
    console.log('grouped actors array');
    console.log(groupedActors);
    let keys = Object.keys(groupedActors);
    // generate the new viewer actor model
    keys.forEach(key => {
      let roleList: ActorModel[] = groupedActors[key];
      // keep unique
      let uniqRoleList = underscore.uniq(roleList, false, (item: ActorModel) => {
        return item.roleName + item.movieName;
      });
      let sortedRoleList = uniqRoleList.sort((aActor: ActorModel, bActor: ActorModel) => {
        if(aActor.movieName < bActor.movieName) return -1;
        if(aActor.movieName > bActor.movieName) return -1;
        return 0;
      });
      viewActors.push(new ViewActorModel(key, sortedRoleList));
    });
    console.log('sorted actors array');
    console.log(viewActors);
    return viewActors;
  }
  projectData(movies: MovieModel[]): ViewActorModel[] {
    let actors: ViewActorModel[] = [];
    for (let aMovie of movies) {
      for (let aRole of aMovie.roles) {
        if (aRole.actor && aRole.name && aMovie.name) {
          this.addActor(actors, new ActorModel(aRole.actor, aRole.name, aMovie.name));
        } else {
          console.log('invalid record');
          console.log(aRole.actor, aRole.name, aMovie);
        }
      }
    }
    return actors;
  }

  addActor(actors: ViewActorModel[], aActor: ActorModel) {
    let actorExisted = false;
    for (let aViewActor of actors) {
      if (aViewActor.actorName === aActor.actor) {
        actorExisted = true;
        let roleExisted = false;
        for (let aRole of aViewActor.roles) {
          if (aRole.roleName === aActor.roleName && aRole.movieName === aActor.movieName) {
            roleExisted = true;
            break;
          }
        }
        if (!roleExisted) {
          aViewActor.roles.push(aActor);
        }
        aViewActor.roles.sort((aActor: ActorModel, bActor: ActorModel) => {
          if(aActor.movieName < bActor.movieName) return -1;
          if(aActor.movieName > bActor.movieName) return -1;
          return 0;
        });
      }
    }
    if (!actorExisted) {
      actors.push(new ViewActorModel(aActor.actor, [aActor]));
    }
  }

  get(): Observable<ViewActorModel[]> {
    // return this.http.get('https://alintacodingtest.azurewebsites.net/api/Movies')
    return this.http.get('assets/data.json')
      .map((res: Response) => {
        return res.json() as MovieModel[];
      }).map((movies: MovieModel[]) => {
        return this.projectData(movies);
      })
      //              .do(data => console.log('server data:', data))  // debug
      .catch(this.handleError);
  }

  /**
    * Handle HTTP error
    */
  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}
