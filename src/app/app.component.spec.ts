import { FormsModule } from '@angular/forms';
import { async, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { AppComponent } from './app.component';
import { NameListService, ViewActorModel } from './name-list.service';


describe('AppComponent', () => {
    let actorWil = {
      "actorName": "Wil Wheaton",
      roles: [
        {
          "actor": "Wil Wheaton",
          "movieName": "Stand By Me",
          "roleName": "Gorgie Lachance"
        },
        {
          "actor": "Wil Wheaton",
          "movieName": "Star Trek",
          "roleName": "Romulan"
        }
      ]
    };
    let actorRiver = {
      "actorName": "River Phoenix",
      roles: [
        {
          "actor": "River Phoenix",
          "movieName": "Stand By Me",
          "roleName": "Chris Chambers"
        }
      ]
    };

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent
            ],
            providers: [
                { provide: NameListService, useValue: new MockNameListService() }
            ]
        }).compileComponents();
    }));

    it('should work',
      async(() => {
        TestBed
          .compileComponents()
          .then(() => {
            let fixture = TestBed.createComponent(AppComponent);
            let appInstance = fixture.debugElement.componentInstance;
            let appDOMEl = fixture.debugElement.nativeElement;
            let mockNameListService =
              fixture.debugElement.injector.get<any>(NameListService) as MockNameListService;
            let nameListServiceSpy = spyOn(mockNameListService, 'get').and.callThrough();

            mockNameListService.returnValue = [actorWil, actorRiver];

            fixture.detectChanges();

            expect(appInstance.nameListService).toEqual(jasmine.any(MockNameListService));
            expect(appDOMEl.querySelectorAll('li').length).toEqual(3);
            expect(appDOMEl.querySelectorAll('li')[0].textContent).toEqual('Gorgie Lachance (Stand By Me)');
            expect(appDOMEl.querySelectorAll('h4')[0].textContent).toEqual('Wil Wheaton');
            expect(appDOMEl.querySelectorAll('li')[2].textContent).toEqual('Chris Chambers (Stand By Me)');
            expect(appDOMEl.querySelectorAll('h4')[1].textContent).toEqual('River Phoenix');
            expect(nameListServiceSpy.calls.count()).toBe(1);
          });

      }));
});

class MockNameListService {

  returnValue: ViewActorModel[];

  get(): Observable<string[]> {
    return Observable.create((observer: any) => {
      observer.next(this.returnValue);
      observer.complete();
    });
  }
}
