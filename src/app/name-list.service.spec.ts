import { TestBed, async, inject } from '@angular/core/testing';
import { BaseRequestOptions, ConnectionBackend, Http, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { Observable } from 'rxjs/Observable';

import { NameListService, ViewActorModel, ActorModel } from './name-list.service';

describe('NameListService', () => {
  let mockData = [
    {
        "name": "Stand By Me",
        "roles": [
            {
                "name": "Gorgie Lachance",
                "actor": "Wil Wheaton"
            },
            {
                "name": "Chris Chambers",
                "actor": "River Phoenix"
            },
            {
                "name": "Teddy Duchamp",
                "actor": "Corey Feldman"
            },
            {
                "name": "Ace Merrill",
                "actor": "Keifer Sutherland"
            },
            {
                "name": "The Writer",
                "actor": "Richard Dreyfuss"
            }
        ]
    },
    {
        "name": "Star Trek",
        "roles": [
            {
                "name": "Romulan",
                "actor": "Wil Wheaton"
            },
            {
                "name": "Kirk",
                "actor": "Chris Pine"
            },
            {
                "name": "Nero",
                "actor": "Eric Bana"
            },
            {
                "name": "Spock",
                "actor": "Leonard Nimoy"
            },
            {
                "name": "Scotty",
                "actor": "Simon Pegg"
            },
            {
                "name": "Amanda Grayson",
                "actor": "Winona Ryder"
            }
        ]
    }
  ];

  let actorWil = new ViewActorModel('Wil Wheaton', [
    new ActorModel('Wil Wheaton', 'Gorgie Lachance', 'Stand By Me'),
    new ActorModel('Wil Wheaton', 'Romulan', 'Star Trek')
  ]);

  let actorRiver = new ViewActorModel('River Phoenix', [
    new ActorModel('River Phoenix', 'Chris Chambers', 'Stand By Me')
  ]);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        NameListService,
        MockBackend,
        BaseRequestOptions,
        {
          provide: Http,
          useFactory: (backend: ConnectionBackend, options: BaseRequestOptions) => new Http(backend, options),
          deps: [MockBackend, BaseRequestOptions]
        }
      ]
    });
  });

  it('should be created', inject([NameListService], (service: NameListService) => {
    expect(service).toBeTruthy();
  }));

  it('should return an Observable when get called', async(() => {
    expect(TestBed.get(NameListService).get()).toEqual(jasmine.any(Observable));
  }));
  
  it('should resolve to list of names when get called', async(() => {
    let nameListService = TestBed.get(NameListService);
    let mockBackend = TestBed.get(MockBackend);

    mockBackend.connections.subscribe((c: any) => {
      c.mockRespond(new Response(new ResponseOptions({ body: mockData })));
    });

    nameListService.get().subscribe((data: any) => {
      expect(data.length).toEqual(10);
      expect(data[0]).toEqual(actorWil);
      expect(data[1]).toEqual(actorRiver);
    });
  }));
});
